def calculate_total_expenses(expenses):
    total_expenses = sum(expenses)
    return total_expenses

def calculate_total_income(incomes):
    total_income = sum(incomes)
    return total_income

def calculate_balance(incomes, expenses):
    total_income = calculate_total_income(incomes)
    total_expenses = calculate_total_expenses(expenses)
    balance = total_income - total_expenses
    return balance

def calculate_cashback(expenses):
    total_cashback = calculate_total_expenses(expenses)*0.02
    return total_cashback

def calculate_deposit_insert(balance):
    total_deposit_insert = round(balance*0.14, 0)
    return total_deposit_insert

def main():
    incomes = []
    expenses = []
    cashbacks = []
    cashbacks_count = 0

    while True:
        choice = input("Выберите действие (добавить доход - 1, добавить расход - 2, подсчитать баланс - 3, получить кешбэк за расходы - 4, начислить проценты по вкладу - 5 выход - 6): ")

        if choice == '1':
            income = float(input(" Введите сумму дохода: "))
            incomes.append(income)
            print(" Доход успешно добавлен.")
        elif choice == '2':
            expense = float(input(" Введите сумму расхода: "))
            expenses.append(expense)
            print(" Расход успешно добавлен.")
        elif choice == '3':
            balance = calculate_balance(incomes, expenses)
            print(" Баланс: ", balance)
        elif choice == '4':
            expenses_count = len(expenses)
            if cashbacks_count < expenses_count:
                cashbacks = calculate_cashback(expenses)
                cashbacks_count+=1
                incomes.append(float(cashbacks))
                balance = calculate_balance(incomes, expenses)
                print(" Кешбэк: ", cashbacks, "добавлен к балансу. \n Текущий баланс: ", balance)
            else:
                print('Весь кешбэк уже выплачен. Добавьте ещё расходов, чтобы поулчить кешбэк.')
        elif choice == '5':
            balance = calculate_balance(incomes, expenses)
            deposit = calculate_deposit_insert(balance)
            incomes.append(float(deposit))
            balance = calculate_balance(incomes, expenses)
            print(" Процент по вкладу: ", deposit, "добавлен к балансу. \n Текущий баланс: ", balance)
        elif choice == '6':
            break
        else:
            print(" Некорректный выбор. Повторите попытку.")

if __name__ == "__main__":
    main()











# def calculate_total_expenses(expenses):
#     total_expenses = sum(expenses)
#     return total_expenses

# def calculate_total_income(incomes):
#     total_income = sum(incomes)
#     return total_income

# def calculate_balance(incomes, expenses):
#     total_income = calculate_total_income(incomes)
#     total_expenses = calculate_total_expenses(expenses)
#     balance = total_income - total_expenses
#     return balance

# def main():
#     incomes = []
#     expenses = []

#     while True:
#         choice = input("Выберите действие (добавить доход - 1, добавить расход - 2, подсчитать баланс - 3, выход - 4): ")

#         if choice == '1':
#             income = float(input("Введите сумму дохода: "))
#             incomes.append(income)
#             print("Доход успешно добавлен.")
#         elif choice == '2':
#             expense = float(input("Введите сумму расхода: "))
#             expenses.append(expense)
#             print("Расход успешно добавлен.")
#         elif choice == '3':
#             balance = calculate_balance(incomes, expenses)
#             print("Баланс: ", balance)
#         elif choice == '4':
#             break
#         else:
#             print("Некорректный выбор. Повторите попытку.")
# # test comm

# if __name__ == "__main__":
#     main()