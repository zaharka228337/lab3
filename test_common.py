import pytest


def calculate_deposit_insert(balance):
    total_deposit_insert = round(balance*0.14, 0)
    return total_deposit_insert





def test_calculate_deposit_insert():
    assert calculate_deposit_insert(1000) == 140
    assert calculate_deposit_insert(1500) == 210
    assert calculate_deposit_insert(2000) == 280
    assert calculate_deposit_insert(2500) == 350
